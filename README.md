# [pypy](https://pkgs.alpinelinux.org/packages?name=pypy&arch=x86_64)

A fast, compliant alternative implementation of Python

* examples with Debian, Ubuntu, Arch, and Fedora in [python-packages-demo/guac](https://gitlab.com/python-packages-demo/guac)
